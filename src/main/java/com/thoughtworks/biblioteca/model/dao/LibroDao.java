package com.thoughtworks.biblioteca.model.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.thoughtworks.biblioteca.model.entities.Libro;

@Stateless
public class LibroDao {

	@PersistenceContext
	private EntityManager em;

	public Libro crear(Libro libro) {
		return em.merge(libro);
	}

	public Libro obtenerPorIsbn(String isbn) {
		return em.find(Libro.class, isbn);
	}

	public List<Libro> obtenerPorTitulo(String titulo) {
		TypedQuery<Libro> consulta = em.createNamedQuery("Libro.buscarPorTitulo", Libro.class);
		consulta.setParameter("titulo", String.format("%%%s%%", titulo));
		return consulta.getResultList();
	}
}
